﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointWhenDead : MonoBehaviour {

     public static int scoreTotal;

    public void AddScore()
    {
        scoreTotal++;
    }

    public static void SetScoreToZero()
    {
        scoreTotal = 0;
    }
}
