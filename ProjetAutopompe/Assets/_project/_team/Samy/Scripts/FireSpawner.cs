﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    public float minTimeBeforeSpawn = 3;
    public float maxTimeBeforeSpawn = 6;
    public float propagationTime = 1.5f;
    public float minX = -2.5f;
    public float maxX = 2.5f;
    public float minZ = -2.5f;
    public float maxZ = 2.5f;

    public LayerMask colliderWith;

    // Use this for initialization
    IEnumerator Start()
    {
        while (true)
        {
            GenerateFireRandomly(objectToSpawn);
            float time = Random.Range(minTimeBeforeSpawn, maxTimeBeforeSpawn);
            yield return new WaitForSeconds(time);
        };
    }

    public void GenerateFireRandomly(GameObject objectToSpawn)
    {
        GameObject fireInst = Instantiate(objectToSpawn);
        fireInst.transform.parent = this.transform;

        Vector3 whereToSpawn =transform.position + new Vector3(Random.Range(minX, maxX), 10000, Random.Range(minZ, maxZ));
        RaycastHit result;
        if (Physics.Raycast(whereToSpawn, Vector3.down, out result, float.MaxValue, colliderWith))
        {
            whereToSpawn = result.point;
        }
        else {
            whereToSpawn.z =
            this.transform.position.z;
        }
        fireInst.transform.position = whereToSpawn;
    }
}