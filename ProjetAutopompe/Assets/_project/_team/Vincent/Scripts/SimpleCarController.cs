﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimpleCarController : MonoBehaviour
{
    public List<AxleInfo> axleInfos; // the information about each individual axle
    public float maxMotorTorque; // maximum torque the motor can apply to wheel
    public float maxSteeringAngle; // maximum steer angle the wheel can have
    public SimpleTouchController leftController;
    public Vector2 touchposition;
    public float motor;
    public bool isMoving = false;

    public void FixedUpdate()
    {
        touchposition = leftController.GetTouchPosition;
        motor = maxMotorTorque * touchposition.y;//Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * touchposition.x;// Input.GetAxis("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                if (motor == 0f)
                {
                    axleInfo.leftWheel.brakeTorque = 10000;
                    axleInfo.rightWheel.brakeTorque = 10000;
                    isMoving = false;
                }else
                {
                    isMoving = true;
                    axleInfo.leftWheel.motorTorque = motor;
                    axleInfo.rightWheel.motorTorque = motor;
                    axleInfo.leftWheel.brakeTorque = 0;
                    axleInfo.rightWheel.brakeTorque = 0;
                }
            }
        }
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
}
