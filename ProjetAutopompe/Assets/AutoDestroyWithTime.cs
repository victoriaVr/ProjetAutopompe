﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyWithTime : MonoBehaviour {

    public GameObject _whatToKill;
    public float _time=60;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _time -= Time.deltaTime;
        if (_time <= 0)
            Destroy(_whatToKill);
	}
}
