﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

    public PipeModel[] pipes;
    public SwitchModel switcher;
    public float anchorRadius = 0.01f;


	// Use this for initialization
	void Start () {
        //foreach (var item in GetNearActivePipes())
        //{
        //    Debug.Log(item.name,item.gameObject);
        //}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public PipeModel GetActivePipeModel()
    {
        for (int i = 0; i < pipes.Length; i++)
        {
            if (pipes[i].IsPipeActive())
            {
                return pipes[i];
            }
        }
        return null;
    }

    public PipeWaterState GetWaterState()
    {
        return GetActivePipeModel().GetComponentInChildren<PipeWaterState>();
    }

    public List<Pipe> GetNearActivePipes()
    {
        PipeModel model = GetActivePipeModel();
        GameObject[] anchors = model.GetAnchors();
        List<Pipe> resultPipes = new List<Pipe>();

        for (int i = 0; i < anchors.Length; i++)
        {
            Pipe[] pipes = GetPipeAtPosition(anchors[i], anchorRadius);
            resultPipes.AddRange(pipes);
        }
        resultPipes.Remove(this);
        return resultPipes;
    }


    public List<Pipe> GetNearActivePipesLinked() {
        List<Pipe> pipes = GetNearActivePipes();
        for (int i = pipes.Count-1; i>= 0; i--)
        {
            List<Pipe> neighbourPipes = pipes[i].GetNearActivePipes();
            if (!neighbourPipes.Contains(this)) {
                pipes.Remove(pipes[i]);
            }

        }
        return pipes;
    }

    internal void SetSwitchActive(bool isActive)
    {
        switcher.enabled = false;
    }

    public static Pipe[] GetPipeAtPosition(GameObject gameObject, float sphereRadius)
    {
        List<Pipe> pipes = new List<Pipe>();
        RaycastHit[] hit = Physics.SphereCastAll(gameObject.transform.position, sphereRadius, Vector3.one* sphereRadius);
        for (int i = 0; i < hit.Length; i++)
        {
            Pipe pipe = hit[i].collider.gameObject.GetComponent<Pipe>();
            if (pipe)
            {
                pipes.Add(pipe);
            }
        }
         return pipes.ToArray();
    }
}
